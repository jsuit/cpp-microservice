#include "crow.h"
#include <boost/optional.hpp>

boost::optional<std::string> getQueryParameter(const crow::request &req, const std::string &name, std::string &value)
{
  auto value0 = req.url_params.get(name);
  if (value0 == nullptr)
  {
    return std::string("missing query parameter ") +  "'" + name + "'";
  }

  if (!strlen(value0))
  {
    return std::string("empty query parameter ") +  "'" + name + "'";
  }
  value.assign(value0);
  return {};
}

int main()
{
  crow::SimpleApp app;

  CROW_ROUTE(app, "/params")
  ([](const crow::request &req) {
    std::ostringstream os;
    os << "Params: " << req.url_params << "\n\n";
    os << "The key 'from' was " << (req.url_params.get("from") == nullptr ? "not " : "") << "found.\n";
    os << "The key 'to' was " << (req.url_params.get("to") == nullptr ? "not " : "") << "found.\n";
    return crow::response{os.str()};
  });

  CROW_ROUTE(app, "/api/xslt")
      .methods("POST"_method)([](const crow::request &req) {

        std::string transactionValue;
        auto check = getQueryParameter(req, "transaction", transactionValue);
        if(check)
        {
          return crow::response(400, check.value());
        }
        std::string fromValue;
        check = getQueryParameter(req, "from", fromValue);
        if(check)
        {
          return crow::response(400, check.value());
        }
        std::string toValue;
        check = getQueryParameter(req, "to", toValue);
        if(check)
        {
          return crow::response(400, check.value());
        }
        return crow::response{req.body};
      });

  app.port(18080).run();
}
